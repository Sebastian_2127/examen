package Reloj;

import java.util.*;

/**
 *
 * @author elsebas
 */
public class RelojDigital extends javax.swing.JFrame implements Runnable{
    String hora, min, seg, mediodia;
    Calendar calendario;
    Thread hilo1;

    /**
     * Creates new form RelojDigital
     */
    public RelojDigital() {
        initComponents();
        hilo1 = new Thread(this);
        hilo1.start();
        setLocationRelativeTo(null);
        setTitle("Reloj Digital");
        setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        EReloj = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        EReloj.setBackground(new java.awt.Color(51, 51, 51));
        EReloj.setFont(new java.awt.Font("Segoe UI Semibold", 0, 48)); // NOI18N
        EReloj.setForeground(new java.awt.Color(0, 102, 255));
        EReloj.setText("jLabel1");
        EReloj.setOpaque(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(EReloj, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(EReloj, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel EReloj;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {
        Thread CT = Thread.currentThread();
        while(CT==hilo1){
            Reloj();
            EReloj.setText(hora+":"+min+":"+seg+" "+mediodia);
            try{
                Thread.sleep(1000);   
            }
            catch(InterruptedException e){}
        } 
    }

    private void Reloj() {
        Calendar calendario = new GregorianCalendar();
        Date Hora = new Date();
        
        calendario.setTime(Hora);
        mediodia=calendario.get(Calendar.AM_PM)==Calendar.AM?"AM":"PM";
        if(mediodia.equals("PM")){
            int H= calendario.get(Calendar.HOUR_OF_DAY)-12;
            hora=H>9?""+H:"0"+H; 
        } else {
            hora = calendario.get(Calendar.HOUR_OF_DAY) > 9 ? "" + calendario.get(Calendar.HOUR_OF_DAY) : "0" + calendario.get(Calendar.HOUR_OF_DAY);
        }
        min = calendario.get(Calendar.MINUTE) > 9 ? "" + calendario.get(Calendar.MINUTE) : "0" + calendario.get(Calendar.MINUTE);
        seg = calendario.get(Calendar.SECOND) > 9 ? "" + calendario.get(Calendar.SECOND) : "0" + calendario.get(Calendar.SECOND);
    }
}
